#include "XShape.h"

const char* XShape::INVALID_HEIGHT_ERROR_MESSAGE = "Can not set X height to 0";
const char XShape::CENTER_X_CHARACTER = 'X';
const char XShape::TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER = '\\';
const char XShape::BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER = '/';
const char XShape::BLANK_CHARACTER = ' ';

XShape::XShape()
	:mHeight(1)
{
}

XShape::~XShape()
{
}

void XShape::SetHeight(const unsigned int height)
{
	// throw exception if given height is zero
	if (height == 0) {
		throw std::invalid_argument(INVALID_HEIGHT_ERROR_MESSAGE);
	}

	mHeight = height;
}

unsigned int XShape::GetHeight() const
{
	return mHeight;
}

std::ostream & XShape::Draw(std::ostream & os) const
{
	//Check to see if we have a center row and column
	bool isOdd = mHeight % 2 != 0;
	unsigned int center = mHeight / 2 + 1;

	for (unsigned int row = 1; row <= mHeight; ++row)
	{
		for (unsigned int column = 1; column <= mHeight; ++column)
		{
			if (isOdd && row == center && column == center)
			{
				os << CENTER_X_CHARACTER;
			}
			else if (row == column)
			{
				os << TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER;
			}
			else if (row == mHeight - column + 1)
			{
				os << BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER;
			}
			else
			{
				os << BLANK_CHARACTER;
			}
		
		}

		os << std::endl;
	}

	return os;
}
