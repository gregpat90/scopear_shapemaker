#include <iostream>
#include "ShapeFactory.h"
#include "ShapeType.h"

const int SUCCESS_RETURN_CODE = 0;
const int NOT_ENOUGH_ARGS_RETURN_CODE = 1;
const int INVALID_ARGS_RETURN_CODE = 2;

const int requiredNumberOfArguments = 3;

using namespace std;

void PrintHelpMessage();

int main(int argc, const char * argv[])
{
	//make sure there is enough args
	if (argc < requiredNumberOfArguments)
	{
		cerr << "Not Enough Arguments" << std::endl;
		PrintHelpMessage();
		return NOT_ENOUGH_ARGS_RETURN_CODE;
	}

	//get shape type from first argument
	int type = strtol(argv[1], NULL, 10);

	if (type != static_cast<int>(ShapeType::CHRISTMAS_TREE) &&
		type != static_cast<int>(ShapeType::X) )
	{
		cerr << "Invalid TYPE" << std::endl;
		PrintHelpMessage();
		return INVALID_ARGS_RETURN_CODE;
	}
	ShapeType shapeType = static_cast<ShapeType>(type);

	//get height from second argument
	unsigned int height = (unsigned int)strtol(argv[2], NULL, 10);
	
	//build the shape
	ShapeFactory factory;
	auto shapeSharedPtr = factory.MakeShape(shapeType);

	// set the height
	try {
		shapeSharedPtr->SetHeight(height);
	}
	catch (std::exception e) {
		cerr << e.what() << std::endl;
		PrintHelpMessage();
		return INVALID_ARGS_RETURN_CODE;
	}

	cout << *shapeSharedPtr << std::endl;

	return SUCCESS_RETURN_CODE;
}

void PrintHelpMessage()
{
	cout << "Usage:" << std::endl;
	cout << "ShapeMaker.exe [Type] [Height]" << std::endl;
	cout << "  Type 1 for Christmas Tree and 2 for X" << std::endl;
	cout << "  Height integer height of the shape" << std::endl;
}