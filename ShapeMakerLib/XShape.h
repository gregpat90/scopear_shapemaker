#pragma once
#include "IShape.h"


//class to describes an "X" shape
class XShape : public IShape
{
public:
	//character used for drawing to ostream
	static const char CENTER_X_CHARACTER;
	static const char TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER;
	static const char BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER;
	static const char BLANK_CHARACTER;

	//Constructs a tree with the minimum height;
	XShape();
	virtual ~XShape();

	//Sets the height of the XShape
	void SetHeight(const unsigned int height) override;

	//Get the height X Shape
	unsigned int GetHeight() const;

private:
	//Error message for when trying to set an invalid height 
	static const char* INVALID_HEIGHT_ERROR_MESSAGE;

	//height of Shape
	unsigned int mHeight;

	//"Draw" the X Shape to the ostream
	virtual std::ostream & Draw(std::ostream & os) const override;

};

