#include "ShapeFactory.h"
#include "ChristmasTree.h"
#include "XShape.h"

std::shared_ptr<IShape> ShapeFactory::MakeShape(ShapeType type)
{
	switch (type)
	{
		case ShapeType::CHRISTMAS_TREE:
			return std::shared_ptr<IShape>(new ChristmasTree());
		case ShapeType::X:
			return std::shared_ptr<IShape>(new XShape());
	}
}
