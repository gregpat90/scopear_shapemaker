#pragma once
#include "ShapeType.h"
#include "IShape.h"

#include <memory>

class ShapeFactory
{
public:
	std::shared_ptr<IShape> MakeShape(ShapeType type);
};
