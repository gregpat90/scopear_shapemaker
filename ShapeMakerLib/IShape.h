#pragma once
#include <iostream>

//The interface for a shape that with configurable height and draw to an ostream
class IShape
{
public:
	virtual ~IShape() { };

	virtual void SetHeight(const unsigned int height) = 0;

	friend std::ostream& operator<<(std::ostream& os, IShape const& shape)
	{
		return shape.Draw(os);
	}

private:
	// Override in derived classes to draw the shape
	virtual std::ostream& Draw(std::ostream& os) const = 0;
};
