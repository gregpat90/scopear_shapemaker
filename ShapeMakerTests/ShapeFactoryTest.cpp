#include "stdafx.h"
#include "CppUnitTest.h"
#include "ShapeFactory.h"
#include "ChristmasTree.h"
#include "XShape.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace FactoryTests
{		
	TEST_CLASS(ShapeFactoryUnitTests)
	{
	public:

		TEST_METHOD(MakeShape_CHRISTMASTREE)
		{
			ShapeFactory factory;

			auto ptr = factory.MakeShape(ShapeType::CHRISTMAS_TREE);
			Assert::IsFalse(!ptr);
			
			auto treePtr = dynamic_cast<ChristmasTree*>(ptr.get());
			Assert::IsNotNull<ChristmasTree>(treePtr);
		}


		TEST_METHOD(MakeShape_X)
		{
			ShapeFactory factory;

			auto ptr = factory.MakeShape(ShapeType::X);
			Assert::IsFalse(!ptr);

			auto treePtr = dynamic_cast<XShape*>(ptr.get());
			Assert::IsNotNull<XShape>(treePtr);
		}
	};
}