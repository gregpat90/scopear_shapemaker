#include "ChristmasTree.h"
#include <stdexcept>

const unsigned int ChristmasTree::MINIMUM_HEIGHT = 3;

const char* ChristmasTree::INVALID_HEIGHT_ERROR_MESSAGE = "Can not set tree height to anything less than the minimum height of 3";

const char ChristmasTree::STAR_CHARACTER = '*';
const char ChristmasTree::LEFT_BRANCH_CHARACTER = '/';
const char ChristmasTree::CENTER_BRANCH_CHARACTER = '|';
const char ChristmasTree::RIGHT_BRANCH_CHARACTER = '\\';
const char ChristmasTree::TRUNK_CHARACTER = 'M';
const char ChristmasTree::BLANK_CHARACTER = ' ';

ChristmasTree::ChristmasTree()
	:mHeight(MINIMUM_HEIGHT)
{
}


ChristmasTree::~ChristmasTree()
{
}

void ChristmasTree::SetHeight(const unsigned int height)
{
	// throw exception if given height it less than MINIMUM_HEIGHT
	if(height < MINIMUM_HEIGHT) {
		throw std::invalid_argument(INVALID_HEIGHT_ERROR_MESSAGE);
	}

	mHeight = height;
}

unsigned int ChristmasTree::GetHeight() const
{
	return mHeight;
}


std::ostream & ChristmasTree::Draw(std::ostream & os) const
{
	//The first row contains the star in the center column
	//the middle row(s) contain the branches
	//the final row contains the truck in the center column 

	//number of columns that make up each side of the tree
	unsigned int columnsPerSide = mHeight - 2;

	unsigned int centerColumn = columnsPerSide + 1;

	//calculate the total number of columns
	unsigned int numOfColumns = 1 + (2 * columnsPerSide);
	
	for (unsigned int row = 1; row <= mHeight; ++row)
	{
		for (unsigned int column = 1; column <= numOfColumns; ++column)
		{
			// draw star in center column of the first row
			if (row == 1)
			{
				if (column == centerColumn)
				{
					os << ChristmasTree::STAR_CHARACTER;
				} 
				else
				{
					os << ChristmasTree::BLANK_CHARACTER;
				}

			}
			//draw a branch with the width based on the row 
			else if (row < mHeight)
			{
				// should have no offset at largest branch
				unsigned int nonBranchOffset = mHeight - row - 1;

				//draw part of the left side branch  
				if (column < centerColumn && column > nonBranchOffset)
				{
					os << ChristmasTree::LEFT_BRANCH_CHARACTER;
				}
				//draw center
				else if (column == centerColumn)
				{
					os << ChristmasTree::CENTER_BRANCH_CHARACTER;
				}
				//draw right side branch 
				else if (column > centerColumn && column <= numOfColumns - nonBranchOffset)
				{
					os << ChristmasTree::RIGHT_BRANCH_CHARACTER;
				}
				else {
					os << ChristmasTree::BLANK_CHARACTER;
				}

			} 
			//draw the trunk in the center column of the last row
			else
			{
				if (column == centerColumn)
				{
					os << ChristmasTree::TRUNK_CHARACTER;
				}
				else
				{
					os << ChristmasTree::BLANK_CHARACTER;
				}

			}
		}

		os << std::endl;
	}

	return os;
}