#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChristmasTree.h"

#include <sstream>
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ShapeTests
{		
	TEST_CLASS(ChristmasTreeUnitTests)
	{
	public:

		TEST_METHOD(SetHeight_Valid)
		{
			ChristmasTree tree;

			unsigned int newTreeHeight = 25;

			tree.SetHeight(newTreeHeight);

			Assert::AreEqual(newTreeHeight, tree.GetHeight());
		}


		TEST_METHOD(SetHeight_ToSmall)
		{

			bool exceptionThrown = false;

			try {
				ChristmasTree tree;

				tree.SetHeight(0);
			}
			catch (std::invalid_argument e) {
				exceptionThrown = true;
			}

			Assert::IsTrue(exceptionThrown);

		}

		TEST_METHOD(OutputOperator)
		{
			ChristmasTree tree;
			tree.SetHeight(5);

			// write the tree to a string stream
			std::stringstream outputStream;
			outputStream << tree;

			std::string outputString = outputStream.str();

			//check for star
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[0]);
			Assert::IsTrue(ChristmasTree::STAR_CHARACTER == outputString[3]);
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[6]);

			//check new line
			Assert::IsTrue('\n' == outputString[7]);

			//check branch
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[8]);
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[9]);
			Assert::IsTrue(ChristmasTree::LEFT_BRANCH_CHARACTER == outputString[10]);
			Assert::IsTrue(ChristmasTree::CENTER_BRANCH_CHARACTER == outputString[11]);
			Assert::IsTrue(ChristmasTree::RIGHT_BRANCH_CHARACTER == outputString[12]);
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[13]);
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[14]);	

			//check trunk
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[32]);
			Assert::IsTrue(ChristmasTree::TRUNK_CHARACTER == outputString[35]);
			Assert::IsTrue(ChristmasTree::BLANK_CHARACTER == outputString[38]);

		}

	};
}