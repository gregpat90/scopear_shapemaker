#pragma once
#include "IShape.h"


//class to describes a Christmas tree with a star on top
class ChristmasTree : public IShape
{
public:
	//Minimum height for a decent looking Christmas tree 
	static const unsigned int MINIMUM_HEIGHT;

	//character used for drawing to ostream
	static const char STAR_CHARACTER;
	static const char LEFT_BRANCH_CHARACTER;
	static const char CENTER_BRANCH_CHARACTER;
	static const char RIGHT_BRANCH_CHARACTER;
	static const char TRUNK_CHARACTER;
	static const char BLANK_CHARACTER;
	
	//Constructs a tree with the minimum height;
	ChristmasTree();
	~ChristmasTree();

	//Sets the height of the Christmas tree including the star on top
	// Throws Exceptions if height is less then MINIMUM_HEIGHT
	void SetHeight(const unsigned int height);
	
	//Get the height of the Christmas tree with including the star on top
	unsigned int GetHeight() const;
	
private:
	//Error message for when trying to set an invalid height 
	static const char* INVALID_HEIGHT_ERROR_MESSAGE;

	//combined height of tree and star
	unsigned int mHeight;

	//"Draw" the Christmas tree to the ostream
	virtual std::ostream & Draw(std::ostream & os) const override;

};

