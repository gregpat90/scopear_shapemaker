#pragma once

enum class ShapeType : int 
{
	CHRISTMAS_TREE = 1,
	X = 2
};