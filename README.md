# README #

This is the project for the part one of the ScopeAR test

It is written in c++ on windows using Visual Studio 2017 community 

# Running App #

1. Open Solution in Visual Studio
1. Build the release version of the solution 
1. Open Command line and navigate to the release directory
1. Run ShapeMaker.exe
