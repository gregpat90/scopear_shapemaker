#include "stdafx.h"
#include "CppUnitTest.h"
#include "XShape.h"

#include <sstream>
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ShapeTests
{		
	TEST_CLASS(XShapeUnitTests)
	{
	public:

		TEST_METHOD(SetHeight_Valid)
		{
			XShape x;

			unsigned int newTreeHeight = 25;

			x.SetHeight(newTreeHeight);

			Assert::AreEqual(newTreeHeight, x.GetHeight());
		}


		TEST_METHOD(SetHeight_ToSmall)
		{

			bool exceptionThrown = false;

			try {
				XShape x;

				x.SetHeight(0);
			}
			catch (std::invalid_argument e) {
				exceptionThrown = true;
			}

			Assert::IsTrue(exceptionThrown);

		}

		TEST_METHOD(OutputOperator_Height3)
		{
			
			XShape x;
			x.SetHeight(3);

			// write the x to a string stream
			std::stringstream outputStream;
			outputStream << x;

			std::string outputString = outputStream.str();

			// should look like
			//  \ /
			//   X
			//  / \

			Assert::IsTrue(XShape::TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER == outputString[0]);
			Assert::IsTrue(XShape::BLANK_CHARACTER == outputString[1]);
			Assert::IsTrue(XShape::BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER == outputString[2]);

			//check new line
			Assert::IsTrue('\n' == outputString[3]);

			Assert::IsTrue(XShape::BLANK_CHARACTER == outputString[4]);
			Assert::IsTrue(XShape::CENTER_X_CHARACTER == outputString[5]);
			Assert::IsTrue(XShape::BLANK_CHARACTER == outputString[6]);

			//check new line
			Assert::IsTrue('\n' == outputString[7]);

			Assert::IsTrue(XShape::BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER == outputString[8]);
			Assert::IsTrue(XShape::BLANK_CHARACTER == outputString[9]);
			Assert::IsTrue(XShape::TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER == outputString[10]);

			//check new line
			Assert::IsTrue('\n' == outputString[11]);

		
		}

		TEST_METHOD(OutputOperator_Height2)
		{

			XShape x;
			x.SetHeight(2);

			// write the x to a string stream
			std::stringstream outputStream;
			outputStream << x;

			std::string outputString = outputStream.str();

			// should look like
			//  \/
			//  /\ 

			Assert::IsTrue(XShape::TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER == outputString[0]);
			Assert::IsTrue(XShape::BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER == outputString[1]);

			//check new line
			Assert::IsTrue('\n' == outputString[2]);

			Assert::IsTrue(XShape::BOTTOM_LEFT_TO_TOP_RIGHT_CHARACTER == outputString[3]);
			Assert::IsTrue(XShape::TOP_LEFT_TO_BOTTOM_RIGHT_CHARACTER == outputString[4]);

			//check new line
			Assert::IsTrue('\n' == outputString[5]);


		}

	};
}